from PIL import Image


def formatter(path):
    sizes = [256, 128, 64]

    img = Image.open(path)
    img.save(f'avatar.WEBP', "WEBP")
    img.convert('RGBA')
    for size in sizes:
        img2 = img.resize((size, size))
        img2.save(f'avatar_{size}.WEBP', "WEBP")


if __name__ == "__main__":
    formatter("avatar.bmp")

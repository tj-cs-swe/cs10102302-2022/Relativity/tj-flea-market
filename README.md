# TJ-flea-market

同济跳蚤市场课程项目

## 运行环境

### python

版本3.10.0

依赖库见[requirements.txt](requirements.txt)

### MySQL

版本8.0.29

数据库名、用户名、密码和端口详见源码下[config.py](config.py)（可修改）

如需远程调试，请参考网络资源，开放root的公网访问权限

```
mysql> CREATE USER 'root'@'%' IDENTIFIED BY 'PASSWORD';
mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;
```

## 安装方法

### python依赖

```
conda create -n tj_market python==3.10.0
conda activate tj_market
pip install -r requirements.txt
或
conda env create --file environment.yml
```
### Redis
版本 Redis-x64-2.8.2402

下载地址：https://github.com/tporadowski/redis/releases

具体参数配置位于 Redis目录下```redis.windows.conf```文件。我们在源码根目录下附上了一份示例配置文件。

端口、数据库编号详见源码下```chat/redismsg.py```（可修改）；默认使用```6379```号端口。

运行项目前需要启动redis服务,在终端中输入```redis-server redis.windows.conf```。

### nginx

我们的项目的websocket提供了https支持。

如您使用nginx反代，不需要额外修改本项目内代码；但请记得在```conf/nginx.conf```中为ws添加相应设置。

同样的，我们在源码根目录下附上了一份示例配置文件。（对应版本为nginx-1.22.0）


## 使用说明

1. 正式运行前，将以下内容清空：

   - 用户头像 ```user/static/resource/user-pic/*```（除```80000000/```和```default_avatar.jpg```外）

   - 商品信息```item/static/resource/item_pic/*```，``````item/static/resource/temp/*``````

   - 用户聊天记录```chat/static/resource/temp/*```
2. ```config.py```中，置```drop_database```为```False```
3. ```app/init_database.py```中，最下方的```init_database()```函数中，将```fake_data()```一行注释
3. ```api/routes.py```中，将验证码注册的“测试用后门”注释

```python
if code == "IEW32DGCBCDZI2B3ELJ7KIAS4HQZMU0M":  # 测试用后门
    return [0, "验证通过"]
```

5. ```python main.py```


## 其它
接口文档：见该部署操作说明文档同目录下的接口文档，或：https://www.apifox.cn/apidoc/shared-af3f347b-6e3e-4257-834b-641ce60fc791

演示网站：https://market.ellye.cn/ （不保证长期在线运行）

